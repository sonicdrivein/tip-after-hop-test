﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using NLog;

namespace TipHandling
{
    public class TipHandler
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public static void CreateDatabase()
        {
            SQLiteConnection.CreateFile(@"C:\TipTest\sonic.db");
            SQLiteConnection connection = new SQLiteConnection(@"Data Source=C:\TipTest\sonic.db;Version=3;");
            logger.Info("Creating Database...");
            connection.Open();
            string tableCreation = "CREATE TABLE tip_dtl (chk_seq int, tip_obj_num int, tip_amount real, pay_reference varchar(20), pay_auth varchar(6), pay_amount real, tip_processed varchar(1));";
            SQLiteCommand command = new SQLiteCommand(tableCreation, connection);
            try
            {
                command.ExecuteNonQuery();
            }
            catch(Exception e)
            {
                logger.Error("Exception in CreateDatabase " + e);
            }

            connection.Close();
        }

        public static void AddTipEntry(int chk_seq, float tipAmount, int tipObjNum, string paymentRef, string paymentAuthCode, float paymentAmount)
        {
            SQLiteConnection connection = new SQLiteConnection(@"Data Source=C:\TipTest\sonic.db;Version=3;");
            connection.Open();
            string tableCreation = "insert into tip_dtl values(@chk, @tipObj, @tipAmt, @payRef, @payAuth, @payAmt, 'F')";
            SQLiteCommand command = new SQLiteCommand(tableCreation, connection);
            command.Parameters.Add("@chk", System.Data.DbType.Int32);
            command.Parameters.Add("@tipObj", System.Data.DbType.Int32);
            command.Parameters.Add("@tipAmt", System.Data.DbType.Decimal);
            command.Parameters.Add("@payRef", System.Data.DbType.String);
            command.Parameters.Add("@payAuth", System.Data.DbType.String);
            command.Parameters.Add("@payAmt", System.Data.DbType.Decimal);
            command.Parameters["@chk"].Value = chk_seq;
            command.Parameters["@tipObj"].Value = tipObjNum;
            command.Parameters["@payRef"].Value = paymentRef;
            command.Parameters["@payAuth"].Value = paymentAuthCode;
            command.Parameters["@tipAmt"].Value = tipAmount;
            command.Parameters["@payAmt"].Value = paymentAmount;
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                logger.Error("Exception in AddTipEntry " + e);
            }


            connection.Close();
        }

        public static void GetTipEntry(int chk_seq, out float tipAmount, out int tipObjNum, out string paymentRef, out string paymentAuthCode, out float paymentAmount, out bool tipProcessed)
        {
            tipAmount = 0.0f;
            tipObjNum = 0;
            paymentRef = "";
            paymentAuthCode = "";
            paymentAmount = 0.0f;
            tipProcessed = false;
            SQLiteConnection connection = new SQLiteConnection(@"Data Source=C:\TipTest\sonic.db;Version=3;");
            connection.Open();
            string tableCreation = "select tip_obj_num, tip_amount, pay_reference, pay_auth, pay_amount, tip_processed from tip_dtl where chk_seq = @chk";
            SQLiteCommand command = new SQLiteCommand(tableCreation, connection);
            command.Parameters.Add("@chk", System.Data.DbType.Int32);
            command.Parameters["@chk"].Value = chk_seq;
            SQLiteDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    tipAmount = float.Parse(reader["tip_amount"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    tipObjNum = Convert.ToInt32(reader["tip_obj_num"]);
                    paymentRef = reader["pay_reference"].ToString();
                    paymentAuthCode = reader["pay_auth"].ToString();
                    paymentAmount = float.Parse(reader["pay_amount"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    if (reader["tip_processed"].ToString() == "T")
                    {
                        tipProcessed = true;
                    }

                }
            }
            catch (Exception e)
            {
                logger.Error("Exception in GetTipEntry " + e);
            }


            connection.Close();
        }

        public static void FlagTipProcessed(int chkSeq)
        {
            SQLiteConnection connection = new SQLiteConnection(@"Data Source=C:\TipTest\sonic.db;Version=3;");
            connection.Open();
            string tableCreation = "update tip_dtl set tip_processed = \'T\' where chk_seq = @chkSeq";
            SQLiteCommand command = new SQLiteCommand(tableCreation, connection);
            command.Parameters.Add("@chkSeq", System.Data.DbType.Int32);
            command.Parameters["@chkSeq"].Value = chkSeq;
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                logger.Error("Exception in FlagTipProcessed " + e);
            }


            connection.Close();
        }
        
        
    }
}
