﻿using System;
using Newtonsoft.Json;

namespace micros_tip_test
{
    public static class JsonHelper
    {
        public static string SerializeJsonToFile<T>(T obj, out string outJson, string prefix = "Object-")
        {
            outJson = JsonConvert.SerializeObject(obj);

            string fileName = string.Format("{0}{1}.json", prefix, DateTime.Now.Ticks);
            System.IO.File.WriteAllText(fileName, outJson);

            return fileName;
        }

        public static string SerializeJsonToFile<T>(T obj, string prefix = "Object-")
        {
            string outJson = "";
            return SerializeJsonToFile(obj, out outJson, prefix);
        }

        public static string SerializeToString<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
