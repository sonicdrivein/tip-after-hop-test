﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using res3700;
using res3700.ResPosApiWebService;

namespace micros_tip_test
{
    static class OrderHelper
    {
        public static void CreateOrder_BurgerAndFries(out ResPosAPI_MenuItem[] items, out ResPosAPI_ComboMeal[] combos)
        {
            items = new ResPosAPI_MenuItem[2];
            items[0] = CreateMenuItem(1003, 1);     // Cheese Burger
            items[1] = CreateMenuItem(9301, 1);     // Med. Fries

            combos = new ResPosAPI_ComboMeal[0];
        }
        private static ResPosAPI_MenuItem CreateMenuItem(int objectNum, int menuLevel)
        {
            ResPosAPI_MenuItem item = new ResPosAPI_MenuItem();
            item.MenuItem = new ResPosAPI_MenuItemDefinition();
            item.MenuItem.MiObjectNum = objectNum;
            item.MenuItem.MiMenuLevel = menuLevel;
            item.Condiments = new ResPosAPI_MenuItemDefinition[0];

            return item;
        }

        public static string CreateReferenceString(ResPosAPI_EPayment payment, string transSequence, string paymentIdentifier)
        {
            string reference = String.Format("{0}{1}|{2}", paymentIdentifier, transSequence, payment.AcctNumber);
            int paddingNeeded = MicrosRESAdapter.TMED_REFERENCE_FIELD_LENGTH - (reference.Length % MicrosRESAdapter.TMED_REFERENCE_FIELD_LENGTH);
            int totalLength = reference.Length + paddingNeeded;
            reference = reference.PadRight(totalLength);
            return reference;
        }

        public static string[] CreateReceiptLines(string receipt)
        {
            string[] receiptLines = receipt.Split('^');
            for (int i = 0; i < receiptLines.Length; i++)
            {
                receiptLines[i] = receiptLines[i] + "\n\r";
            }

            return receiptLines;
        }

        public static decimal StringToDecimal(string sDecimal)
        {
            decimal dValue = 0.0M;
            decimal.TryParse(sDecimal, out dValue);
            return dValue;
        }

    }

    public class SeatPaymentFactory
    {
        public static ResPosAPI_SeatPayment[] CreateSeatPayments(int seat, ResPosAPI_EPayment payment, int tenderObjectNum)
        {
            ResPosAPI_SeatPayment[] seatPayments = new ResPosAPI_SeatPayment[1];
            seatPayments[0] = new ResPosAPI_SeatPayment();
            seatPayments[0].SeatNum = seat;
            seatPayments[0].pSeatPayments = new ResPosAPI_TmedDetailItemEx[1];
            seatPayments[0].pSeatPayments[0] = new ResPosAPI_TmedDetailItemEx();
            seatPayments[0].pSeatPayments[0].TmedEPayment = payment;
            seatPayments[0].pSeatPayments[0].TmedObjectNum = tenderObjectNum;
            return seatPayments;
        }
    }

    public class SeatTmedVouchersFactory
    {
        public static ResPosAPI_SeatTmedVoucher[] CreateSeatTmedVouchers(int seat, string[] receiptLines)
        {
            ResPosAPI_SeatTmedVoucher[] vouchers = new ResPosAPI_SeatTmedVoucher[1];
            vouchers[0] = new ResPosAPI_SeatTmedVoucher();
            vouchers[0].SeatNum = seat;
            vouchers[0].ppVoucherOutput = receiptLines;
            return vouchers;
        }
    }

    public static class CheckFactory
    {
        public static ResPosAPI_GuestCheck CreateGuestCheck(int checkNumber, int checkSequence, bool dispatched)
        {
            ResPosAPI_GuestCheck check = new ResPosAPI_GuestCheck();
            check.CheckEmployeeObjectNum = MicrosRESAdapter.API_EMPLOYEE;
            check.CheckNum = checkNumber;
            check.CheckSeq = checkSequence;
            check.CheckStatusBits = 0;
            check.CheckStatusBits |= MicrosRESAdapter.CHECKSTATUS_SUPPRESS_CHK_PRINT;
            check.CheckStatusBits |= MicrosRESAdapter.CHECKSTATUS_KEEP_CHK_CLOSED_DATE_TIME;
            if (!dispatched)
            {
                // If the check hasn't yet been scanned out, then leave it open and don't print
                // the receipt yet.
                check.CheckStatusBits |= MicrosRESAdapter.CHECKSTATUS_DONOT_CLOSE_CHECK;
                check.CheckStatusBits |= MicrosRESAdapter.CHECKSTATUS_SUPPRESS_VOUCHER_PRINT;
            }

            return check;
        }
    }

}
