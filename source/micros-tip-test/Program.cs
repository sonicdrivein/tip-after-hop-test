﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using res3700;
using res3700.ResPosApiWebService;
using Newtonsoft.Json;
using NLog;
using System.IO;

namespace micros_tip_test
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        static MicrosRESAdapter res = new MicrosRESAdapter("http://192.168.100.99/ResPosApiWeb/ResPosApiWeb.asmx");

        static void Main(string[] args)
        {
            //Order variables:
            int seat = 1;
            string transSequence = "123456";
            int employee = 9998;
            int chkSeq = 13;
            float tipAmt = 5.50f;

            //Get check summary for totals and check num/seq
            ResPosAPI_CheckSummaryWithSeats pSummary = new ResPosAPI_CheckSummaryWithSeats();
            res.GetCheckSummaryWithSeats(chkSeq, "", ref employee, ref pSummary);

            string totalAmt = pSummary.CheckTotalPayment;

            //Create a check object for payment
            ResPosAPI_GuestCheck checkForPayment = new ResPosAPI_GuestCheck()
            {
                CheckNum = pSummary.CheckNum,
                CheckSeq = pSummary.CheckSeq,
                CheckDateToFire = DateTime.Now.AddMinutes(1),
                CheckEmployeeObjectNum = employee,
                CheckID = "",
                CheckOrderType = 6,
                CheckRevenueCenterObjectNum = 0
            };

            //Create the E paymenbt object
            ResPosAPI_EPayment payment = new ResPosAPI_EPayment();
            payment.AccountDataSource = EAccountDataSource.MANUALLY_KEYED_NO_CARD_READER;
            payment.AccountType = EAccountType.ACCOUNT_TYPE_UNDEFINED;
            payment.AcctNumber = "4012000000001881"; // Test card number, partially masked
            payment.AuthorizationCode = "123456";
            payment.BaseAmount = totalAmt;
            payment.ExpirationDate = DateTime.Now.AddYears(1);
            payment.PaymentCommand = EPaymentDirective.CREDIT_AUTH_AND_PAY_VOID_CASH_TENDER;
            payment.TipAmount = tipAmt.ToString();

            //Create objects required for ResAPI Call - AddPaymentBySeat
            ResPosAPI_SeatPayment[] seatPayments = SeatPaymentFactory.CreateSeatPayments(seat, payment, MicrosRESAdapter.TENDER_OBJECT_AUTOTENDER);
            string reference = OrderHelper.CreateReferenceString(payment, transSequence, MicrosRESAdapter.POPS_PAYMENT_GATEWAY);
            seatPayments[0].pSeatPayments[0].TmedReference = reference;
            string[] receiptLines = null;
            ResPosAPI_SeatTmedVoucher[] vouchers = SeatTmedVouchersFactory.CreateSeatTmedVouchers(seat, receiptLines);
            string[] printLines = null;
            ResPosAPI_SeatSummary[] seatResponses = new ResPosAPI_SeatSummary[0];
             
            //Add payment to check
            res.AddPaymentBySeat(ref checkForPayment,
                ref seatPayments,
                ref printLines,
                ref seatResponses,
                ref vouchers
                );
        }


    }
}
