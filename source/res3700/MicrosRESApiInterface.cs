﻿using res3700.ResPosApiWebService;

namespace res3700
{
    public interface MicrosRESApiInterface
    {
        void CalculateTransactionTotals(
            ref ResPosAPI_MenuItem[] ppMenuItems,
            ref ResPosAPI_ComboMeal[] ppComboMeals,
            ref ResPosAPI_SvcCharge pSvcCharge,
            ref ResPosAPI_Discount pSubtotalDiscount,
            int revenueCenter, short orderType, int employeeNumber,
            ref ResPosAPI_TotalsResponse pTotalsResponse);

        void PostTransaction(
            ref ResPosAPI_GuestCheck pGuestCheck,
            ref ResPosAPI_MenuItem[] ppMenuItems,
            ref ResPosAPI_ComboMeal[] ppComboMeals,
            ref ResPosAPI_SvcCharge pServiceChg,
            ref ResPosAPI_Discount pSubTotalDiscount,
            ref ResPosAPI_TmedDetailItem pTmedDetail,
            ref ResPosAPI_TotalsResponse pTotalsResponse);

        void AddToExistingCheck(
            ref ResPosAPI_GuestCheck pGuestCheck,
            ref ResPosAPI_MenuItem[] ppMenuItems,
            ref ResPosAPI_ComboMeal[] ppComboMeals,
            ref ResPosAPI_SvcCharge pServiceChg,
            ref ResPosAPI_Discount pSubTotalDiscount,
            ref ResPosAPI_TmedDetailItem pTmedDetail,
            ref ResPosAPI_TotalsResponse pTotalsResponse);

        void AddPaymentBySeat(
            ref ResPosAPI_GuestCheck pGuestCheck,
            ref ResPosAPI_SeatPayment[] ppCheckSeatPayment,
            ref string[] ppCheckPrintLines,
            ref ResPosAPI_SeatSummary[] ppSeatResponse,
            ref ResPosAPI_SeatTmedVoucher[] ppSeatVoucherOutput);

        void AddVouchersOnExistingCheck(
            ref ResPosAPI_GuestCheck pGuestCheck,
            ref ResPosAPI_TmedDetailItemEx pSvtTmedDetail,
            ref ResPosAPI_TmedDetailItemEx pGiftCardTmedDetail,
            ref ResPosAPI_GiftCardItem[] ppItems);

        void GetPaymentBySeat(
            ref ResPosAPI_GuestCheck pGuestCheck,
            bool bGetVoidedPayments,
            ref ResPosAPI_SeatPayment[] ppCheckSeatPayment);

        void VoidTransaction(ref ResPosAPI_GuestCheck pGuestCheck);

        void GetOpenChecks(
            int employeeObjectNum,
            ref ResPosAPI_CheckSummary[] ppCheckSummaryArray);

        void LockDispatchEx(
            ref ResPosAPI_GuestCheck pGuestCheck,
            bool bLock,
            string appID);

        void GetCheckSummaryWithSeats(
            int pGuestCheck,
            string empId,
            ref int empObj,
            ref ResPosAPI_CheckSummaryWithSeats pSummary);

    }
}
