﻿using res3700.ResPosApiWebService;

namespace res3700
{
    

    public class MicrosRESAdapter : MicrosRESApiInterface
    {
        public static readonly int CHECKSTATUS_VIP_CHECK = 0x2;
        public static readonly int CHECKSTATUS_DONOT_CLOSE_CHECK = 0x20;
        public static readonly int CHECKSTATUS_KEEP_CHK_CLOSED_DATE_TIME = 0x40;
        public static readonly int CHECKSTATUS_SUPPRESS_CHK_PRINT = 0x80;
        public static readonly int CHECKSTATUS_SUPPRESS_VOUCHER_PRINT = 0x100;

        public static readonly int TENDER_OBJECT_AUTOTENDER = 201;
        public static readonly int TMED_REFERENCE_FIELD_LENGTH = 20;

        public static string POPS_PAYMENT_GATEWAY = "E:";

        public static readonly int API_EMPLOYEE = 9997;

        private ResPosApiWebService.ResPosApiWebService res;

        public MicrosRESAdapter(string url)
        {
            res = new ResPosApiWebService.ResPosApiWebService();
            res.Url = url;
        }

        public void CalculateTransactionTotals(ref ResPosAPI_MenuItem[] ppMenuItems, ref ResPosAPI_ComboMeal[] ppComboMeals,
            ref ResPosAPI_SvcCharge pSvcCharge, ref ResPosAPI_Discount pSubtotalDiscount, int revenueCenter, short orderType,
            int employeeNumber, ref ResPosAPI_TotalsResponse pTotalsResponse)
        {
            res.CalculateTransactionTotals(ref ppMenuItems, ref ppComboMeals, ref pSvcCharge, ref pSubtotalDiscount, revenueCenter, orderType, employeeNumber, ref pTotalsResponse);
        }

        public void PostTransaction(ref ResPosAPI_GuestCheck pGuestCheck, ref ResPosAPI_MenuItem[] ppMenuItems,
            ref ResPosAPI_ComboMeal[] ppComboMeals, ref ResPosAPI_SvcCharge pServiceChg,
            ref ResPosAPI_Discount pSubTotalDiscount, ref ResPosAPI_TmedDetailItem pTmedDetail,
            ref ResPosAPI_TotalsResponse pTotalsResponse)
        {
            res.PostTransaction(ref pGuestCheck, ref ppMenuItems, ref ppComboMeals, ref pServiceChg, ref pSubTotalDiscount, ref pTmedDetail, ref pTotalsResponse);
        }

        public void AddToExistingCheck(ref ResPosAPI_GuestCheck pGuestCheck, ref ResPosAPI_MenuItem[] ppMenuItems,
            ref ResPosAPI_ComboMeal[] ppComboMeals, ref ResPosAPI_SvcCharge pServiceChg,
            ref ResPosAPI_Discount pSubTotalDiscount, ref ResPosAPI_TmedDetailItem pTmedDetail,
            ref ResPosAPI_TotalsResponse pTotalsResponse)
        {
            res.AddToExistingCheck(ref pGuestCheck, ref ppMenuItems, ref ppComboMeals, ref pServiceChg, ref pSubTotalDiscount, ref pTmedDetail, ref pTotalsResponse);
        }

        public void AddPaymentBySeat(ref ResPosAPI_GuestCheck pGuestCheck, ref ResPosAPI_SeatPayment[] ppCheckSeatPayment,
            ref string[] ppCheckPrintLines, ref ResPosAPI_SeatSummary[] ppSeatResponse,
            ref ResPosAPI_SeatTmedVoucher[] ppSeatVoucherOutput)
        {
            res.AddPaymentBySeat(ref pGuestCheck, ref ppCheckSeatPayment, ref ppCheckPrintLines, ref ppSeatResponse, ref ppSeatVoucherOutput);
        }

        public void AddVouchersOnExistingCheck(ref ResPosAPI_GuestCheck pGuestCheck, ref ResPosAPI_TmedDetailItemEx pSvtTmedDetail,
            ref ResPosAPI_TmedDetailItemEx pGiftCardTmedDetail, ref ResPosAPI_GiftCardItem[] ppItems)
        {
            res.AddVouchersOnExistingCheck(ref pGuestCheck, ref pSvtTmedDetail, ref pGiftCardTmedDetail, ref ppItems);
        }

        public void GetPaymentBySeat(ref ResPosAPI_GuestCheck pGuestCheck, bool bGetVoidedPayments,
            ref ResPosAPI_SeatPayment[] ppCheckSeatPayment)
        {
            res.GetPaymentBySeat(ref pGuestCheck, bGetVoidedPayments, ref ppCheckSeatPayment);
        }

        public void VoidTransaction(ref ResPosAPI_GuestCheck pGuestCheck)
        {
            res.VoidTransaction(ref pGuestCheck);
        }

        public void GetOpenChecks(int employeeObjectNum, ref ResPosAPI_CheckSummary[] ppCheckSummaryArray)
        {
            res.GetOpenChecks(employeeObjectNum, ref ppCheckSummaryArray);
        }

        public void LockDispatchEx(ref ResPosAPI_GuestCheck pGuestCheck, bool bLock, string appID)
        {
            res.LockDispatchEx(ref pGuestCheck, bLock, appID);
        }

        public void GetCheckSummaryWithSeats(int chkSeq, string empId, ref int empObj, ref ResPosAPI_CheckSummaryWithSeats pSummary)
        {
           res.GetCheckSummaryWithSeats(chkSeq, empId, ref empObj, ref pSummary);
        }
    }
}
