﻿using System;
using Newtonsoft.Json;

namespace res3700.MenuCrossReference
{
    public class MicrosOrderAheadMenuItem
    {
        // This item is used in the new order ahead cross reference
        // and has enough information for bi-directional lookup
        public MicrosOrderAheadMenuItem()
        {
            PLU = "";
            ObjectNum = "";
            MainLevel = 0;
            SubLevel = 0;
        }

        [JsonProperty("PLU", Required = Required.Always)]
        public string PLU
        {
            get;
            set;
        }

        [JsonProperty("objNum", Required = Required.Always)]
        public string ObjectNum
        {
            get;
            set;
        }

        [JsonProperty("mainLevel", Required = Required.AllowNull)]
        public Int32 MainLevel
        {
            get;
            set;
        }

        [JsonProperty("subLevel", Required = Required.AllowNull)]
        public Int32 SubLevel
        {
            get;
            set;
        }

        [JsonProperty("entree", Required = Required.Default)]
        public string Entree
        {
            get;
            set;
        }
    }

    public class MicrosOrderAheadDiscountItem : IEquatable<MicrosOrderAheadDiscountItem>
    {
        // This item is used in the new order ahead cross reference
        // and has enough information for bi-directional lookup
        public MicrosOrderAheadDiscountItem()
        {
            PLU = "";
            ObjectNum = "";
            Name = "";
            SLU = "";
        }

        [JsonProperty("PLU", Required = Required.Always)]
        public string PLU { get; set; }

        [JsonProperty("objNum", Required = Required.Always)]
        public string ObjectNum { get; set; }

        [JsonProperty("name", Required = Required.Default)]
        public string Name { get; set; }

        [JsonProperty("slu", Required = Required.Default)]
        public string SLU { get; set; }

        public bool Equals(MicrosOrderAheadDiscountItem discountItem)
        {
            if (discountItem == null)
                return false;

            if (this.ObjectNum == discountItem.ObjectNum
                && this.Name == discountItem.Name
                && this.PLU == discountItem.PLU
                && this.SLU == discountItem.SLU)
            {
                return true;
            }

            return false;
        }
    }

    public class MicrosLegacyMenuItem : IComparable
    {
        // This item is used in the old (pre-order ahead) cross reference
        // and only has enough information for one-directional lookup, from
        // object/price -> PLU.
        public MicrosLegacyMenuItem(string objNum, string priceLevel)
        {
            this.objNum = objNum;
            this.priceLevel = priceLevel;
        }

        public MicrosLegacyMenuItem(string standardPLUId)
        {
            this.standardPLUId = standardPLUId;
            priceLevel = "1";
        }

        public MicrosLegacyMenuItem()
        {
            objNum = "";
            priceLevel = "";
            standardPLUId = "";
        }

        [JsonProperty("standardPLUId", Required = Required.Always)]
        public string standardPLUId
        {
            get;
            set;
        }

        [JsonProperty("objNum", Required = Required.Always)]
        public string objNum
        {
            get;
            set;
        }

        [JsonProperty("priceLevel", Required = Required.Always)]
        public string priceLevel
        {
            get;
            set;
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            int value = 0;
            if (obj is MicrosLegacyMenuItem)
            {
                MicrosLegacyMenuItem other = (MicrosLegacyMenuItem)obj;

                int objNumCompare = other.objNum.CompareTo(objNum);
                if (objNumCompare == 0)
                {
                    value = other.priceLevel.CompareTo(priceLevel);
                }
                else
                {
                    return objNumCompare;
                }
            }
            else
            {
                throw new ArgumentException("obj is not the same type as this instance");
            }

            return value;
        }

        #endregion
    }
}
