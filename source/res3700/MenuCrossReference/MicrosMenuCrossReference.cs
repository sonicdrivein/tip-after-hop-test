﻿using Newtonsoft.Json;

namespace res3700.MenuCrossReference
{
    public class MicrosOrderAheadMenuCrossReference
    {
        // This is the new format of the menu cross reference. The old cross
        // reference can only handle one-directional lookup from Object Num ->
        // PLU for order confirmation. This cross reference is designed for
        // bi-directional lookup for both order confirmation and order ahead.
        public MicrosOrderAheadMenuCrossReference()
        {
            count = 0;
            results = new MicrosOrderAheadMenuItem[0];
        }

        [JsonProperty("count", Required = Required.Always)]
        public int count
        {
            get;
            set;
        }

        [JsonProperty("results", Required = Required.Always)]
        public MicrosOrderAheadMenuItem[] results
        {
            get;
            set;
        }

        [JsonProperty("discountsCount", Required = Required.Default)]
        public int discountsCount
        {
            get;
            set;
        }

        [JsonProperty("discountsResults", Required = Required.Default)]
        public MicrosOrderAheadDiscountItem[] discountsResults
        {
            get;
            set;
        }
    }

    public class MicrosLegacyMenuCrossReference
    {
        // This is the old format of the menu cross reference. It was
        // designed only for one-directional lookup from object num/price level
        // to PLU for order confirmation. Since it doesn't split between main
        // and sub menu levels, it cannot be used for looking up the other direction.
        public MicrosLegacyMenuCrossReference()
        {
            count = 0;
            results = new MicrosLegacyMenuItem[0];
        }

        [JsonProperty("count", Required = Required.Always)]
        public int count
        {
            get;
            set;
        }

        [JsonProperty("results", Required = Required.Always)]
        public MicrosLegacyMenuItem[] results
        {
            get;
            set;
        }
    }

}
